from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
import tensorflow as tf

train_data = [
  "I enjoy coffee.",
  "I enjoy tea.",
  "I dislike milk.",
  "I am going to the supermarket later this morning for some coffee."
]

test_data = [
  "Enjoy coffee this morning.",
  "I enjoy going to the supermarket.",
  "Want some milk for your coffee?"
]
#parameters
num_words = 1000
oov_token = '<UNK>'
pad_type = 'post'
trunc_type = 'post'

#tokenizer word in text
tokenizer  = Tokenizer(num_words=1000, oov_token=oov_token)
tokenizer.fit_on_texts(train_data)

#get vocabulary
print('vocabulary: ',tokenizer.word_index)

#texts to sequences of numerics
train_sequences = tokenizer.texts_to_sequences(train_data)
train_matrix = tokenizer.texts_to_matrix(train_data)

print(train_sequences)
#max len of sequences
maxlen = max([len(x) for x in train_sequences])
print('maxlen: ',maxlen)
#padding
train_padded = pad_sequences(train_sequences, padding=pad_type,truncating=trunc_type,maxlen=maxlen)
print(train_padded)

print("Training sequences data type:", type(train_sequences))
print("Padded Training sequences data type:", type(train_padded))

test_sequences = tokenizer.texts_to_sequences(test_data)
test_padded = pad_sequences(test_sequences, padding=pad_type, truncating=trunc_type, maxlen=maxlen)







#generate tf.data.Dataset from numpy
labels = np.array([1,0,0])
train_dataset = tf.data.Dataset.from_tensor_slices((test_padded, labels))
#batch data
data = train_dataset.batch(2)

data1 = next(iter(data))
print(train_dataset)
print(data1)